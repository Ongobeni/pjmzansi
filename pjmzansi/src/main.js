import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import PrimeVue from 'primevue/config';
import InputText from 'primevue/inputtext';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.config.productionTip = false
Vue.use(VueSweetalert2);
new Vue({
  router,
  VueAxios,
  axios,
  PrimeVue,
  InputText,
  render: h => h(App)
}).$mount('#app')

